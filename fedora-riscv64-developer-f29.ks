# Kickstart file for Fedora RISC-V (riscv64) Developer F29

#repo --name="koji-override-0" --baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f29/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc America/New_York
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd

bootloader --location=none --disabled

zerombr
clearpart --all --initlabel --disklabel=gpt
part / --fstype="ext4" --size=7168

# Halt the system once configuration has finished.
poweroff

%packages
@core
@buildsys-build
@base-x
@hardware-support

kernel
kernel-core
kernel-devel
kernel-modules
kernel-modules-extra
linux-firmware
fedora-bbl

openssh
openssh-server
glibc-langpack-en
lsof
nano
openrdate
chrony
systemd-udev
vim-minimal
neovim
screen
hostname
bind-utils
htop
tmux
strace
pciutils
nfs-utils
ethtool
rsync
hdparm
git
moreutils
rpmdevtools
fedpkg
mailx
mutt
patchutils
ninja-build
cmake
extra-cmake-modules
elfutils
gdisk
util-linux
gparted
parted
fpaste
vim-common
hexedit
koji-builder
mc
evemu
lftp
mtr
wget
aria2
incron
emacs
vim
neofetch
bash-completion
zsh
nvme-cli
pv
dtc
axel
bc
bison
elfutils-devel
flex
m4
net-tools
openssl-devel
perl-devel
perl-generators
pesign
xterm
fluxbox
elinks
lynx
awesome
midori
dillo
epiphany
i3
sway
pcmanfm
entr
cowsay
ack
the_silver_searcher
tldr
ncdu
colordiff
prettyping
yum
qemu-guest-agent
iptables-services
autoconf
automake
gettext
nnn
gdb
libtool
texinfo
# Below packages are needed for creating disk images via koji-builder
livecd-tools
python-imgcreate-sysdeps
python3-imgcreate
python2-pyparted
python3-pyparted
isomd5sum
python2-isomd5sum
python3-isomd5sum
pykickstart
python2-kickstart
python3-kickstart
python2-ordered-set
python3-ordered-set
appliance-tools
pycdio
qemu-img
# end of creating disk image packages list
# Add dejavu fonts
dejavu-fonts-common
dejavu-lgc-sans-fonts
dejavu-lgc-sans-mono-fonts
dejavu-lgc-serif-fonts
dejavu-sans-fonts
dejavu-sans-mono-fonts
dejavu-serif-fonts
# end of dejavu fonts
%end

%post
# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled rawhide updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f29/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/29/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/29/latest/riscv64/
enabled=1
gpgcheck=0

[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f29/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/29/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/29/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f29/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/29/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/29/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora-riscv.tranquillity.se/repos/f29-build/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedorapeople.org/groups/risc-v/disk-images/readme.txt

Fedora/RISC-V
-------------
Koji:               http://fedora-riscv.tranquillity.se/koji/
SCM:                http://fedora-riscv.tranquillity.se:3000/
Distribution rep.:  http://fedora-riscv.tranquillity.se/repos-dist/
Koji internal rep.: http://fedora-riscv.tranquillity.se/repos/
EOF
%end

# EOF
