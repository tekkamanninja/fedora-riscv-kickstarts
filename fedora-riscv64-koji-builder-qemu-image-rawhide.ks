# Kickstart file for Fedora RISC-V (riscv64) Minimal Rawhide

#repo --name="koji-override-0" --baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-riscv
rootpw --plaintext fedora_rocks!
firewall --enabled --ssh
timezone --utc America/New_York
selinux --enforcing
services --enabled=sshd,NetworkManager,chronyd,haveged

bootloader --location=none --extlinux

zerombr
clearpart --all --initlabel --disklabel=gpt
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=1536

# Halt the system once configuration has finished.
poweroff

%packages
@core
@hardware-support
-initial-setup-gui
-glibc-all-langpacks
-iproute-tc
-trousers
-xkeyboard-config
-usb_modeswitch
-ipw*
-iwl*

kernel
kernel-core
kernel-modules
linux-firmware
extlinux-bootloader
uboot-tools
freedom-u540-c000-bootloader
# Remove this in %post
dracut-config-generic
-dracut-config-rescue

glibc-langpack-en
nano
chrony
haveged
qemu-guest-agent

-grubby
grubby-deprecated

# No longer in @core since 2018-10, but needed for livesys script
initscripts
chkconfig

# Lets resize / on first boot
#dracut-modules-growroot

koji-builder
appliance-tools

%end

%post
# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled rawhide updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular rawhide-modular

dnf -y remove dracut-config-generic

# systemd on no-SMP boots (i.e. single core) sometimes timeout waiting for storage
# devices. After entering emergency prompt all disk are mounted.
# For more information see:
# https://www.suse.com/support/kb/doc/?id=7018491
# https://www.freedesktop.org/software/systemd/man/systemd.mount.html
# https://github.com/systemd/systemd/issues/3446
# We modify /etc/fstab to give more time for device detection (the problematic part)
# and mounting processes. This should help on systems where boot takes longer.
sed -i 's|noatime|noatime,x-systemd.device-timeout=300s,x-systemd.mount-timeout=300s|g' /etc/fstab

# Fedora 31
# https://fedoraproject.org/wiki/Changes/DisableRootPasswordLoginInSshd
cat > /etc/rc.d/init.d/livesys << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.
### BEGIN INIT INFO
# X-Start-Before: display-manager chronyd
### END INIT INFO

. /etc/rc.d/init.d/functions

useradd -c "Fedora RISCV User" riscv
echo fedora_rocks! | passwd --stdin riscv > /dev/null
usermod -aG wheel riscv > /dev/null

exit 0
EOF

chmod 755 /etc/rc.d/init.d/livesys
/sbin/restorecon /etc/rc.d/init.d/livesys
/sbin/chkconfig --add livesys

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
enabled=1
gpgcheck=0


[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora.riscv.rocks/repos/rawhide/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is 'fedora_rocks!'.
root password logins are disabled in SSH starting Fedora 31.
User 'riscv' with password 'fedora_rocks!' in 'wheel' group is provided.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedoraproject.org/wiki/Architectures/RISC-V

Fedora/RISC-V
-------------
Koji:               http://fedora.riscv.rocks/koji/
SCM:                http://fedora.riscv.rocks:3000/
Distribution rep.:  http://fedora.riscv.rocks/repos-dist/
Koji internal rep.: http://fedora.riscv.rocks/repos/
EOF

# setup systemd to boot to the right runlevel
echo -n "Setting default runlevel to multiuser text mode"
rm -f /etc/systemd/system/default.target
ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
echo .

# Remove machine-id on pre generated images
rm -f /etc/machine-id
touch /etc/machine-id

# remove random seed, the newly installed instance should make it's own
rm -f /var/lib/systemd/random-seed

%end

# EOF
