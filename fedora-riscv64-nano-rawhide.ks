# Kickstart file for Fedora RISC-V (riscv64) Nano Rawhide

#repo --name="koji-override-0" --baseurl=http://fedora-riscv.tranquillity.se/repos-dist/rawhide/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc America/New_York
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd

bootloader --location=none --disabled

zerombr
clearpart --all --initlabel --disklabel=gpt
part / --fstype="ext4" --size=2048

# Halt the system once configuration has finished.
poweroff

%packages --excludedocs --instLangs=en
@core

kernel
kernel-core
kernel-devel
kernel-modules
kernel-modules-extra
linux-firmware
-fedora-bbl

chrony
%end

%post
# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled rawhide updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/rawhide/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
enabled=1
gpgcheck=0

[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/rawhide/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/rawhide/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora-riscv.tranquillity.se/repos/rawhide/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedorapeople.org/groups/risc-v/disk-images/readme.txt

Fedora/RISC-V
-------------
Koji:               http://fedora-riscv.tranquillity.se/koji/
SCM:                http://fedora-riscv.tranquillity.se:3000/
Distribution rep.:  http://fedora-riscv.tranquillity.se/repos-dist/
Koji internal rep.: http://fedora-riscv.tranquillity.se/repos/
EOF
%end

# EOF
