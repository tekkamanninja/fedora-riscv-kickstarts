# Kickstart file for Fedora RISC-V (riscv64, vic7100) Developer Rawhide

#repo --name="koji-override-0" --baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-starfive
rootpw --plaintext starfive
firewall --enabled --ssh
timezone --utc Asia/Shanghai
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd,haveged,lightdm --disabled=lm_sensors,libvirtd,gdm

bootloader --location=none --extlinux

zerombr
clearpart --all --initlabel --disklabel=gpt
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=12288

# Halt the system once configuration has finished.
poweroff

%packages --ignoremissing
@core
@buildsys-build
@base-x
@hardware-support
@rpm-development-tools
@c-development
@development-tools
@xfce-desktop
#@gnome-desktop

# missing packages of xfce-desktop
-abrt-desktop
-dnfdragora-updater
-gnome-abrt
-python3-yui

# packages below cannot work well at present (May 16 2021)
#-i3lock
#-i3status
#-i3
#-swaybg
#-sway

# Starfive specific packages setting
kernel-vic7100
kernel-vic7100-core
kernel-vic7100-devel
kernel-vic7100-modules
kernel-vic7100-modules-extra
-kernel
-kernel-core
-kernel-devel
-kernel-modules
-kernel-modules-extra
bcm4343b0-firmware
starfive-arts
# Starfive specific packages setting
linux-firmware
opensbi-unstable
extlinux-bootloader
uboot-tools
uboot-images-riscv64
# Remove this in %post
dracut-config-generic
-dracut-config-rescue

openssh
openssh-server
glibc-langpack-en
glibc-static
lsof
nano
openrdate
chrony
systemd-udev
vim-minimal
neovim
screen
hostname
bind-utils
htop
tmux
strace
pciutils
nfs-utils
ethtool
rsync
hdparm
git
tig
mercurial
breezy
moreutils
rpmdevtools
fedpkg
mailx
mutt
patchutils
ninja-build
cmake
extra-cmake-modules
elfutils
gdisk
util-linux
gparted
parted
fpaste
vim-common
hexedit
koji-builder
mc
evemu
lftp
mtr
traceroute
wget
aria2
incron
emacs
vim
neofetch
bash-completion
zsh
tcsh
nvme-cli
pv
dtc
axel
bc
bison
elfutils-devel
flex
m4
net-tools
openssl-devel
perl-devel
perl-generators
pesign
xterm
#fluxbox
elinks
lynx
#awesome
midori
dillo
epiphany
#i3
#sway
pcmanfm
entr
cowsay
ack
the_silver_searcher
tldr
ncdu
colordiff
prettyping
qemu-guest-agent
iptables-services
autoconf
autoconf-archive
automake
gettext
nnn
gdb
libtool
texinfo
policycoreutils
policycoreutils-python-utils
setools-console
coreutils
setroubleshoot-server
audit
selinux-policy
selinux-policy-targeted
execstack
stress-ng
python3-pyelftools
inxi
# Below packages are needed for creating disk images via koji-builder
livecd-tools
python-imgcreate-sysdeps
python3-imgcreate
python3-pyparted
isomd5sum
python3-isomd5sum
pykickstart
python3-kickstart
python3-ordered-set
appliance-tools
pycdio
qemu-img
nbdkit
nbd
# end of creating disk image packages list
dosfstools
btrfs-progs
e2fsprogs
f2fs-tools
jfsutils
mtd-utils
ntfsprogs
udftools
xfsprogs
kpartx
libguestfs-tools-c
rpkg
binwalk
bloaty
bpftool
kernel-tools
perf
python3-perf
libgpiod
libgpiod-c++
libgpiod-devel
libgpiod-utils
python3-libgpiod
i2c-tools
i2c-tools-eepromer
i2c-tools-perl
libi2c
libi2c-devel
python3-i2c-tools
spi-tools
# Add gcc packages
cpp
gcc
gcc-c++
gcc-gdb-plugin
gcc-gfortran
gcc-go
gcc-plugin-devel
libatomic
libatomic-static
libgcc
libgfortran
libgfortran-static
libgo
libgo-devel
libgo-static
libgomp
libstdc++
libstdc++-devel
libstdc++-static
gcc-gdc
libgphobos
libgphobos-static
pax-utils
gcc-gnat
libgnat
libgnat-devel
libgnat-static
usbutils
haveged
# end of gcc packages
# Add dejavu fonts
dejavu-fonts-all
dejavu-lgc-sans-fonts
dejavu-lgc-sans-mono-fonts
dejavu-lgc-serif-fonts
dejavu-sans-fonts
dejavu-sans-mono-fonts
dejavu-serif-fonts
# end of dejavu fonts

-grubby
grubby-deprecated

# No longer in @core since 2018-10, but needed for livesys script
initscripts
chkconfig

# Lets resize / on first boot
#dracut-modules-growroot

dnscrypt-proxy
meson
cloud-utils-growpart
iperf3
sysstat
fio
memtester
fuse-sshfs
zstd
xz
NetworkManager-tui
cheat
ddrescue
glances
python3-psutil

# Add dependencies (BR) for kernel, gcc, gdb, binutils, rpm, util-linux, glibc,
# bash and coreutils
audit-libs-devel
bzip2-devel
dblatex
dbus-devel
dejagnu
docbook5-style-xsl
dwarves
expat-devel
fakechroot
file-devel
gd-devel
gettext-devel
glibc-all-langpacks
ima-evm-utils-devel
isl-devel
libacl-devel
libarchive-devel
libattr-devel
libbabeltrace-devel
libcap-devel
libcap-ng-devel
libdb-devel
libpng-devel
libselinux-devel
libuser-devel
libutempter-devel
libzstd-devel
lua-devel
ncurses-devel
pam-devel
pcre2-devel
perl
popt-devel
python3-devel
python3-langtable
python3-sphinx
readline-devel
rpm-devel
sharutils
source-highlight-devel
systemd-devel
texinfo-tex
texlive-collection-latex
texlive-collection-latexrecommended
zlib-static
# end of dependencies (BR)

dos2unix
fwts
acpica-tools
glib
glib2
dkms
expect
openssl
gnutls-utils
iw
info
wireless-tools
wireless-regdb
jq
sysfsutils
golang
golang-bin
golang-src
bmap-tools
flashrom

glib-devel
glib2-devel
json-c-devel
libbsd-devel
zlib-devel
xz-devel
brotli-devel
libuv-devel
libnghttp2-devel
libicu-devel
libX11-devel
libXtst-devel
libXt-devel
libXrender-devel
libXrandr-devel
libXi-devel
libXext-devel
cups-devel
fontconfig-devel
alsa-lib-devel
freetype-devel
libdwarf-devel
gnulib-devel

# Additional packages for vic7100 seeed
ckermit
minicom

NetworkManager-wifi

NetworkManager-bluetooth
bluez
python3-bluez
python3-crypto
python3-evdev
python3-entrypoints
python3-iso8601
python3-jmespath
python3-pexpect
python3-pyserial
python3-wheel
python3-sh
python3-asn1crypto

xorg-x11-apps
avahi
%end

%post
# TODO: BCMDHD driver is too unstable to enable will removed after driver debug
cat > /etc/modprobe.d/bcmdhd-blacklist.conf << EOF
blacklist brcmfmac
blacklist bcmdhd
EOF

# TODO: make sure the dracut add dw_mmc-pltfm but remove bcmdhd in initrd
cat > /etc/dracut.conf.d/vic7100.conf << EOF
add_drivers+="dw_mmc-pltfm"
omit_drivers+="brcmfmac bcmdhd"
EOF

# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled rawhide updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular rawhide-modular

dnf -y remove dracut-config-generic

# remove window managers cannot run correctly at present (May 16 2021)
# dnf -y remove i3 i3status i3lock swaybg sway

# Re-generating RPM database because the target releasever may use different format of database
# rpm --rebuilddb

# change kernel to vic7100 specific
#dnf -y update
#dnf -y remove kernel kernel-core kernel-devel kernel-modules kernel-modules-extra
#dnf -y install kernel-vic7100 kernel-vic7100-core kernel-vic7100-devel kernel-vic7100-modules kernel-vic7100-modules-extra

# dracut -f --kver `ls /boot/ | grep -i vmlinuz | grep -i riscv64 | sed -e 's/vmlinuz-//g'` --add-drivers dw_mmc-pltfm
dracut -f --kver `ls /boot/ | grep -i vmlinuz | grep -i riscv64 | sed -e 's/vmlinuz-//g'`
# dracut -f

sed -i \
        -e 's/^ui/# ui/g'	\
        -e 's/^menu autoboot/# menu autoboot/g'	\
        -e 's/^menu hidden/# menu hidden/g'	\
        -e 's/^totaltimeout/# totaltimeout/g'	\
        -e 's/rhgb quiet/rhgb console=tty0 console=ttyS0,115200 earlycon=sbi rootwait stmmaceth=chain_mode:1 selinux=0/g'   \
        -e 's/	addappend/	append/g' \
        /boot/extlinux/extlinux.conf

ln -sf ./starfive/starlight-ui.png /usr/share/backgrounds/default.png
ln -sf ../starfive/starlight-ui.png /usr/share/backgrounds/images/default.png
ln -sf ../starfive/starlight-ui.png /usr/share/backgrounds/images/default-5_4.png
ln -sf ../starfive/starlight-ui.png /usr/share/backgrounds/images/default-16_9.png
ln -sf ../starfive/starlight-ui.png /usr/share/backgrounds/images/default-16_10.png

# systemd on no-SMP boots (i.e. single core) sometimes timeout waiting for storage
# devices. After entering emergency prompt all disk are mounted.
# For more information see:
# https://www.suse.com/support/kb/doc/?id=7018491
# https://www.freedesktop.org/software/systemd/man/systemd.mount.html
# https://github.com/systemd/systemd/issues/3446
# We modify /etc/fstab to give more time for device detection (the problematic part)
# and mounting processes. This should help on systems where boot takes longer.
sed -i 's|noatime|noatime,x-systemd.device-timeout=300s,x-systemd.mount-timeout=300s|g' /etc/fstab

# Fedora 31
# https://fedoraproject.org/wiki/Changes/DisableRootPasswordLoginInSshd
cat > /etc/rc.d/init.d/livesys << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.
### BEGIN INIT INFO
# X-Start-Before: display-manager chronyd
### END INIT INFO

. /etc/rc.d/init.d/functions

useradd -c "Fedora RISCV User" riscv
echo starfive | passwd --stdin riscv > /dev/null
usermod -aG wheel riscv > /dev/null

exit 0
EOF

chmod 755 /etc/rc.d/init.d/livesys
/sbin/restorecon /etc/rc.d/init.d/livesys
/sbin/chkconfig --add livesys

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
enabled=1
gpgcheck=0

[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora.riscv.rocks/repos/rawhide/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is 'starfive'.
root password logins are disabled in SSH starting Fedora 31.
User 'riscv' with password 'starfive' in 'wheel' group is provided.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedoraproject.org/wiki/Architectures/RISC-V

Fedora/RISC-V
-------------
Koji:               http://fedora.riscv.rocks/koji/
SCM:                http://fedora.riscv.rocks:3000/
Distribution rep.:  http://fedora.riscv.rocks/repos-dist/
Koji internal rep.: http://fedora.riscv.rocks/repos/
EOF

# Remove machine-id on pre generated images
rm -f /etc/machine-id
touch /etc/machine-id

# remove random seed, the newly installed instance should make it's own
rm -f /var/lib/systemd/random-seed

%end


# EOF
