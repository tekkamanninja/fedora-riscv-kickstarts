# Kickstart file for Fedora RISC-V (riscv64) GNOME F31

#repo --name="koji-override-0" --baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f31/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc America/New_York
selinux --enforcing
services --enabled=sshd,NetworkManager,chronyd,haveged --disabled=lm_sensors

bootloader --location=none --extlinux

zerombr
clearpart --all --initlabel --disklabel=gpt
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=7680

# Halt the system once configuration has finished.
poweroff

%packages
@core
@buildsys-build
@base-x
@hardware-support
@rpm-development-tools
@c-development
@development-tools
@gnome-desktop

kernel
kernel-core
kernel-devel
kernel-modules
kernel-modules-extra
linux-firmware
fedora-bbl
fedora-bbl-nopayload
opensbi-unstable
opensbi-unstable-fedora
opensbi-unstable-images-riscv64
extlinux-bootloader
uboot-tools
uboot-images-riscv64

openssh
openssh-server
glibc-langpack-en
glibc-static
lsof
nano
openrdate
chrony
systemd-udev
vim-minimal
neovim
screen
hostname
bind-utils
htop
tmux
strace
pciutils
nfs-utils
ethtool
rsync
hdparm
git
moreutils
rpmdevtools
fedpkg
mailx
mutt
patchutils
ninja-build
cmake
extra-cmake-modules
elfutils
gdisk
util-linux
gparted
parted
fpaste
vim-common
hexedit
koji-builder
mc
evemu
lftp
mtr
wget
aria2
incron
emacs
vim
neofetch
bash-completion
zsh
nvme-cli
pv
dtc
axel
bc
bison
elfutils-devel
flex
m4
net-tools
openssl-devel
perl-devel
perl-generators
pesign
xterm
fluxbox
elinks
lynx
awesome
midori
dillo
epiphany
i3
sway
pcmanfm
entr
cowsay
ack
the_silver_searcher
tldr
ncdu
colordiff
prettyping
qemu-guest-agent
iptables-services
autoconf
autoconf-archive
automake
gettext
nnn
gdb
libtool
texinfo
policycoreutils
policycoreutils-python-utils
setools-console
coreutils
setroubleshoot-server
audit
selinux-policy
selinux-policy-targeted
execstack
stress-ng
python3-pyelftools
inxi
# Below packages are needed for creating disk images via koji-builder
livecd-tools
python-imgcreate-sysdeps
python3-imgcreate
python3-pyparted
isomd5sum
python3-isomd5sum
pykickstart
python3-kickstart
python3-ordered-set
appliance-tools
pycdio
qemu-img
# end of creating disk image packages list
dosfstools
btrfs-progs
e2fsprogs
f2fs-tools
jfsutils
mtd-utils
ntfsprogs
udftools
xfsprogs
kpartx
libguestfs-tools-c
rpkg
binwalk
bpftool
kernel-tools
perf
python3-perf
# Add gcc packages
cpp
gcc
gcc-c++
gcc-gdb-plugin
gcc-gfortran
gcc-go
gcc-plugin-devel
libatomic
libatomic-static
libgcc
libgfortran
libgfortran-static
libgo
libgo-devel
libgo-static
libgomp
libstdc++
libstdc++-devel
libstdc++-static
gcc-gdc
libgphobos
libgphobos-static
pax-utils
gcc-gnat
libgnat
libgnat-devel
libgnat-static
usbutils
haveged
# end of gcc packages
# Add dejavu fonts
dejavu-fonts-common
dejavu-lgc-sans-fonts
dejavu-lgc-sans-mono-fonts
dejavu-lgc-serif-fonts
dejavu-sans-fonts
dejavu-sans-mono-fonts
dejavu-serif-fonts
# end of dejavu fonts

-grubby
grubby-deprecated
%end

%post
# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular

# systemd on no-SMP boots (i.e. single core) sometimes timeout waiting for storage
# devices. After entering emergency prompt all disk are mounted.
# For more information see:
# https://www.suse.com/support/kb/doc/?id=7018491
# https://www.freedesktop.org/software/systemd/man/systemd.mount.html
# https://github.com/systemd/systemd/issues/3446
# We modify /etc/fstab to give more time for device detection (the problematic part)
# and mounting processes. This should help on systems where boot takes longer.
sed -i 's|noatime|noatime,x-systemd.device-timeout=300s,x-systemd.mount-timeout=300s|g' /etc/fstab

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f31/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/f31/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/f31/latest/riscv64/
enabled=1
gpgcheck=0

[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f31/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/f31/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/f31/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora-riscv.tranquillity.se/repos-dist/f31/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/f31/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/f31/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora-riscv.tranquillity.se/repos/f31/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedorapeople.org/groups/risc-v/disk-images/readme.txt

Fedora/RISC-V
-------------
Koji:               http://fedora-riscv.tranquillity.se/koji/
SCM:                http://fedora-riscv.tranquillity.se:3000/
Distribution rep.:  http://fedora-riscv.tranquillity.se/repos-dist/
Koji internal rep.: http://fedora-riscv.tranquillity.se/repos/
EOF
%end

# EOF
